<?php
session_start();

$valid_types = array('jpg', 'png','pdf','xlsx','docx');

if (isset($_FILES['userfile'])) {
    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) 
    {
        $filename = basename($_FILES['userfile']['name']);
        $ext = substr($filename, 1 + strrpos($filename, '.'));
        // strpos --  Возвращает позицию первого вхождения подстроки

        if (!in_array($ext, $valid_types)) 
        {
            echo 'Error: Invalid file type.';
        }               
        else 
        {
            $uploadfile = $filename;
            move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
            echo "<img src='$uploadfile' alt='$filename' title='$filename' />";
        }
    } else {
        echo 'Error: empty file.';
    }
} else {
	echo '
	<form enctype="multipart/form-data" method="post">
	Send this file: <input name="userfile" type="file">
	<input type="submit" value="Send File">
	</form>';
}

?>
