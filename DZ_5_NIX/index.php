<?php
require_once "form.php";

$s = [];
if(!empty($_POST))
{
    if(isset($_POST['btn']) && ($_POST['btn'] == "get_number"))
    {
        $_SESSION['number'] = $_REQUEST['number'];
        $number = $_SESSION['number'];
        $generateNumber = rand(5,8);
        if($number < 5)
        {
            echo 'Число меньше 5';
        }
        elseif($number > 8)
        {
            echo 'Число больше 8';
        }
        elseif($number == $generateNumber)
        {
            echo 'Вы угадали! Поздравляем!';
        }
        elseif($number > 4 && $number < 9)
        {
            echo 'Почти угадали, пробуйте еще';
        }
        else
        {
            echo 'Упс... скорее всего вы ввели не число...';
        }
    }
}


?>