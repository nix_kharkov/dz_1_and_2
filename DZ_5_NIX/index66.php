<?php

echo '<h3> 1. Написать программу, которая выводит простые числа, т.е. делящиеся без остатка только на себя и на 1.</h3>';
for ($i = 2; $i < 10; $i++) 
{
    for ($j = 2; $j < $i; $j++) 
    {
        if (($i % $j) == 0) 
        {
            $k = "Число не простое";
        }
    }
    if ($k == "Число простое") 
    {
        echo '<pre>';
        echo "Число $i - является простым";
        echo '</pre>';

    }
    else 
       $k = "Число простое";
}

echo '<h3> 2. Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100. </h3>';
$someCountOfNumber2 = 100;
$array2 = [];
$count2 = 0;
for($i = 0; $i < $someCountOfNumber2; $i++)
{
    $array2[] .= rand(0,100);
    if($array2[$i] % 2 == 0)
    {
        $count2++;
    }
}
// echo '<pre>';
// print_r($array2);
// echo '</pre>';
echo "<p> Количество четных чисел - $count2 </p>";

echo '<h3> 3. Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5). </h3>';
$someCountOfNumber3 = 100;
$array3 = [];
$count3_1 = 0;
$count3_2 = 0;
$count3_3 = 0;
$count3_4 = 0;
$count3_5 = 0;
for($i = 0; $i < $someCountOfNumber3; $i++)
{
    $array3[] .= rand(1,5);
    if($array3[$i] == 1)
       $count3_1++;
    else if($array3[$i] == 2)
       $count3_2++;
    else if($array3[$i] == 3)
       $count3_3++;
    else if($array3[$i] == 4)
       $count3_4++;
    else if($array3[$i] == 5)
       $count3_5++;
    else
       echo 'Что-то полшло не так... :-(';    
}
// echo '<pre>';
// print_r($array3);
// echo '</pre>';
echo "<p> Число 1 сгенерировалось $count3_1 раз </p>";
echo "<p> Число 2 сгенерировалось $count3_2 раз </p>";
echo "<p> Число 3 сгенерировалось $count3_3 раз </p>";
echo "<p> Число 4 сгенерировалось $count3_4 раз </p>";
echo "<p> Число 5 сгенерировалось $count3_5 раз </p>";

echo '<h3> 4. Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек. </h3>';

echo "<table border='1'";
for ($a = 153; $a < 256; $a += 51) 
{
    echo "<tr>";
    for ($b = 0; $b < 51; $b += 51) 
    {
          for ($c = 0; $c < 255; $c += 51) 
          {
              echo "<td style=background-color:rgb($a,$b,$c) width=100 height=50></td>";
          }
    }
}
echo "</tr>";
echo "</table>";

echo '<h3> 5. В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень). </h3>';
$month = rand(1,12);
//echo $month . '<br>';
if($month > 0 && $month < 3 || $month == 12)
    echo "Это зимний месяц";
else if($month > 2 && $month < 6)
    echo "Это весенний месяц";
else if($month > 5 && $month < 9)
    echo "Это летний месяц";
else if($month > 8 && $month < 12)
    echo "Это осенний месяц";
else
    echo "Опа.... где-то ошибка...";

echo '<h3> 6. Дана строка, состоящая из символов, например, \'abcde\'. Проверьте, что первым символом этой строки является буква \'a\'. 
              Если это так - выведите \'да\', в противном случае выведите \'нет\'. </h3>';
$str6 = 'asdasda';
echo ($str6[0] == 'a') ? '<p> Да! </p>' : '<p> Нет! </p>';
// if($str6[0] == 'a')
//    echo '<p> Да! </p>';
// else
//    echo '<p> Нет! </p>';

echo '<h3> 7. Дана строка с цифрами, например, \'12345\'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. 
              Если это так - выведите \'да\', в противном случае выведите \'нет\'. </h3>';
$str7 = '12345';
echo ($str7[0] == '1') ? '<p> Да! </p>' : ($str7[0] == '2') ? '<p> Да! </p>' : ($str7[0] == '3') ? '<p> Да! </p>' : '<p> Нет! </p>';
// if($str7[0] == '1')
//    echo '<p> Да! </p>';
// elseif($str7[0] == '2')
//    echo '<p> Да! </p>';
// elseif($str7[0] == '3')
//    echo '<p> Да! </p>';
// else
//    echo '<p> Нет! </p>';

echo '<h3> 8. Если переменная test равна true, то выведите \'Верно\', иначе выведите \'Неверно\'. 
              Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else. </h3>';
$test8 = false;
if($test8)
{
    echo '<p> Верно! </p>';
}
else
{
    echo '<p> Неверно! </p>';
}
echo ($test8) ? '<p> Верно! </p>' : '<p> Неверно! </p>';

echo '<h3> 9. Дано два массива рус и англ [\'пн\', \'вт\', \'ср\', \'чт\', \'пт\', \'сб\', \'вс\']. 
              Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. 
              Сделать через if else и через тернарку. </h3>';
$dayOfWeekRU = ['Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье'];
$dayOfWeekEN = ['Sunday','Monday','Tuesday','Wensday','Thursday','Friday','Saturday'];
$lang = 'en';
if($lang == 'ru') {
    foreach($dayOfWeekRU as $ruDay) {
        echo '<pre>';
        print($ruDay);
        echo '</pre>';
    }
}
else {
    foreach($dayOfWeekEN as $enDay) {
        echo '<pre>';
        print($enDay);
        echo '</pre>';
    }
}
echo '<pre>';
$lang == 'ru' ? print_r($dayOfWeekRU): print_r($dayOfWeekEN);
echo '</pre>';

echo '<h3> 10. В переменной cloсk лежит число от 0 до 59 – это минуты. 
               Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). 
               Тернарка и if else. </h3>';
$clock = rand(0, 59);
echo $clock;
if($clock >= 0 && $clock < 15)
    echo '<p> Попадает в первую четверть </p>';
else if($clock >= 15 && $clock < 30)
    echo '<p> Попадает во вторую четверть </p>';
else if($clock >= 30 && $clock < 45)
    echo '<p> Попадает в третью четверть </p>';
else if($clock >= 45 && $clock <= 59)
    echo '<p> Попадает в четвертую четверть </p>';
else
    echo '<p> Скорее всего ваше число больше или равно 60.... </p>';

// echo ($clock >= 0 && $clock < 15) ? '<p> Попадает в первую четверть </p>' : 
//       ($clock >= 15 && $clock < 30) ? '<p> Попадает во вторую четверть </p>' : 
//       ($clock >= 30 && $clock < 45) ? '<p> Попадает в третью четверть </p>' :
//       ($clock >= 45 && $clock <= 59) ? '<p> Попадает в четвертую четверть </p>' : '<p> Скорее всего ваше число больше или равно 60.... </p>';

// <-===============================================================================->
echo '<h3> 1. Реализовать свою функцию count() </h3>';

$array11 = [22,33,44,55];

function count_for(array $arr)
{
    $countFor = 0;
    for ($countFor = 0; $arr[$countFor] != null; $countFor++)
    { 
        
    }
    return $countFor;
}
echo count_for($array11);

echo '<br>';

function count_while(array $arr)
{
    $countWhile = 0;
    while($arr[$countWhile] != null)
    {
       $countWhile++;
    }
    return $countWhile;
}
echo count_while($array11);

echo '<br>';

function count_foreach(array $arr)
{
    $countForeach = 0;
    foreach ($arr as $item)
    {
        $countForeach++;
    }
    return $countForeach;
}
echo count_foreach($array11);

echo '<br>';

function count_do_while(array $arr)
{
    $countDoWhile = -1;
    do
    {
       $countDoWhile++;
    }while($arr[$countDoWhile] != null);
    return $countDoWhile;
}
echo count_do_while($array11);

echo '<h3> 2. Дан массив [\'Alex\', \'Vanya\', \'Tanya\', \'Lena\', \'Tolya\']. 
              Развернуть этот массив в обратном направлении. </h3>';

$array22 = ['Alex','Vanya','Tanya','Lena','Tolya'];

function array_reverse_for(array $arr)
{
    $newArr = [];
    for ($i = count_for($arr); $i > 0; $i--) 
    { 
        $newArr[] .= $arr[$i-1];
    }
    return $newArr;
}
echo '<pre>';
print_r(array_reverse_for($array22));
echo '</pre>';

function array_reverse_while(array $arr)
{
    $newArr = [];
    $countArr = count_while($arr);
    while ($countArr != 0) 
    { 
        $newArr[] .= $arr[$countArr-1];
        $countArr--;
    }
    return $newArr;
}
echo '<pre>';
print_r(array_reverse_while($array22));
echo '</pre>';

function array_reverse_foreach(array $arr)
{
    $newArr = [];
    $countArr = count_foreach($arr);
    foreach($arr as $item)
    {
        $newArr[] .= $arr[$countArr-1]; 
        $countArr--;
    }
    return $newArr;
}
echo '<pre>';
print_r(array_reverse_foreach($array22));
echo '</pre>';

function array_reverse_do_while(array $arr)
{
    $newArr = [];
    $countArr = count_do_while($arr);
    do
    {
        $newArr[] .= $arr[$countArr - 1];
        $countArr--;
    }while($countArr != 0);
    return $newArr;
}
echo '<pre>';
print_r(array_reverse_do_while($array22));
echo '</pre>';

echo '<h3> 3. Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]. Развернуть этот массив в обратном направлении. </h3>';
$array33 = [44, 12, 11, 7, 1, 99, 43, 5, 69];

echo '<pre>';
print_r(array_reverse_for($array33));
echo '</pre>';

echo '<pre>';
print_r(array_reverse_while($array33));
echo '</pre>';

echo '<pre>';
print_r(array_reverse_foreach($array33));
echo '</pre>';

echo '<pre>';
print_r(array_reverse_do_while($array33));
echo '</pre>';

echo '<h3> 4. Дана строка $str = \'Hi I am ALex\'. Развернуть строку в обратном направлении. </h3>';
$str4 = 'Hi I am ALex';

function str_count_while(string $str)
{
    $countWhile = 0;
    while($str[$countWhile] != null)
    {
        $countWhile++;
    }
    return $countWhile;
}

function str_reverse_for(string $str)
{
    $newStr = '';
    for ($i = str_count_while($str); $i > 0; $i--) 
    { 
        $newStr .= $str[$i-1];
    }
    return $newStr;
}
echo str_reverse_for($str4);

echo '<br>';

function str_reverse_while(string $str)
{
    $newStr = '';
    $countChars = str_count_while($str);
    while ($countChars != 0) 
    { 
        $newStr .= $str[$countChars-1];
        $countChars--;
    }
    return $newStr;
}
echo str_reverse_while($str4);

echo '<br>';

function str_reverse_foreach(string $str)
{
    $newArr = [];
    $newStr = '';
    $countChars = str_count_while($str);
    for ($i=0; $i < $countChars; $i++)
    { 
        $newArr[] .= $str[$i];
    }
    foreach($newArr as $item)
    {
        $newStr .= $newArr[$countChars-1]; 
        $countChars--;
    }
    return $newStr;
}
echo str_reverse_foreach($str4);

echo '<br>';

function str_reverse_do_while(string $str)
{
    $newStr = '';
    $countChars = str_count_while($str);
    do
    {
        $newStr .= $str[$countChars - 1];
        $countChars--;
    }while($countChars != 0);
    return $newStr;
}
print(str_reverse_do_while($str4));

echo '<h3> 5. Дана строка $str = \'Hi I am ALex\', сделать ее с маленьких букв. </h3>';
$str5 = 'Hi I am ALex';

function str_to_lower_for(string $str)
{
    $arr_upper = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $arr_lower = ['a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $newStr = '';
    $count1 = str_count_while($str);
    $count2 = count_for($arr_upper);
    for ($i=0; $i < $count1; $i++) 
    { 
        for ($j=0; $j < $count2; $j++) 
        { 
            if($str[$i] == $arr_upper[$j])
            {
                $newStr .= $arr_lower[$j];
            }
            elseif($str[$i] == ' ')
            {
                $newStr .= $str[$i];
            }
            elseif($str[$i] == $arr_lower[$j])
            {
                $newStr .= $arr_lower[$j];
            }         
        }
    }
    return $newStr;
}
print str_to_lower_for($str5);

echo '<br>';

function str_to_lower_while(string $str)
{
    $arr_upper = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $arr_lower = ['a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $newStr = '';
    $countChars = str_count_while($str);
    $countCharsConst = $countChars;
    $countArrItem = count_while($arr_upper);
    $countConst = $countArrItem;
    while($countChars != 0) 
    { 
        $countArrItem = $countConst;
        while ($countArrItem != 0) 
        { 
            if($str[$countCharsConst - $countChars] == $arr_upper[$countConst - $countArrItem])
            {
                $newStr .= $arr_lower[$countConst - $countArrItem];
            }
            elseif($str[$countCharsConst - $countChars] == ' ')
            {
                $newStr .= $str[$countCharsConst - $countChars];
            }
            elseif($str[$countCharsConst - $countChars] == $arr_lower[$countConst - $countArrItem])
            {
                $newStr .= $arr_lower[$countConst - $countArrItem];
            }     
            $countArrItem--;    
        }
        $countChars--;
    }
    return $newStr;
}
print str_to_lower_while($str5);

echo '<br>';

function str_to_lower_foreach(string $str)
{
    $arrChars = [
        'A' => 'a',
        'B' => 'b',
        'C' => 'c',
        'D' => 'd',
        'E' => 'e',
        'F' => 'f',
        'G' => 'g',
        'H' => 'h',
        'I' => 'i',
        'J' => 'j',
        'K' => 'k',
        'L' => 'l',
        'M' => 'm',
        'N' => 'n',
        'O' => 'o',
        'P' => 'p',
        'Q' => 'q',
        'R' => 'r',
        'S' => 's',
        'T' => 't',
        'U' => 'u',
        'V' => 'v',
        'W' => 'w',
        'X' => 'x',
        'Y' => 'y',
        'Z' => 'z'
    ];
    $newStr = '';
    $newArr = [];
    $countChars = str_count_while($str);
    for ($i=0; $i < $countChars; $i++)
    { 
        $newArr[] .= $str[$i];
    }
    foreach ($newArr as $item) 
    {
        foreach ($arrChars as $upper => $lower) 
        {
            if($item == $upper)
            {
                $newStr .= $lower;
            }
            elseif($item == ' ')
            {
                $newStr .= $item;
            }
            elseif($item == $lower)
            {
                $newStr .= $lower;
            }
        }      
    }
    return $newStr;
}
print str_to_lower_foreach($str5);

echo '<br>';

function str_to_lower_do_while(string $str)
{
    $arr_upper = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $arr_lower = ['a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $newStr = '';
    $countChars = str_count_while($str);
    $countCharsConst = $countChars;
    $countArrItem = count_while($arr_upper);
    $countConst = $countArrItem;
    do
    { 
        $countArrItem = $countConst;
        do
        { 
            if($str[$countCharsConst - $countChars] == $arr_upper[$countConst - $countArrItem])
            {
                $newStr .= $arr_lower[$countConst - $countArrItem];
            }
            elseif($str[$countCharsConst - $countChars] == ' ')
            {
                $newStr .= $str[$countCharsConst - $countChars];
            }
            elseif($str[$countCharsConst - $countChars] == $arr_lower[$countConst - $countArrItem])
            {
                $newStr .= $arr_lower[$countConst - $countArrItem];
            }     
            $countArrItem--;    
        }while ($countArrItem != 0);
        $countChars--;
    }while($countChars != 0);
    return $newStr;
}
print str_to_lower_do_while($str5);

echo '<br>';

echo '<h3> 6. Дана строка $str = \'Hi I am ALex\', сделать все буквы большие. </h3>';
$str6 = 'Hi I am ALex';

function str_to_upper_for(string $str)
{
    $arr_upper = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $arr_lower = ['a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $newStr = '';
    $count1 = str_count_while($str);
    $count2 = count_for($arr_lower);
    for ($i=0; $i < $count1; $i++) 
    { 
        for ($j=0; $j < $count2; $j++) 
        { 
            if($str[$i] == $arr_lower[$j])
            {
                $newStr .= $arr_upper[$j];
            }
            elseif($str[$i] == ' ')
            {
                $newStr .= $str[$i];
            }
            elseif($str[$i] == $arr_upper[$j])
            {
                $newStr .= $arr_upper[$j];
            }         
        }
    }
    return $newStr;
}
print str_to_upper_for($str6);

echo '<br>';

function str_to_upper_while(string $str)
{
    $arr_upper = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $arr_lower = ['a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $newStr = '';
    $countChars = str_count_while($str);
    $countCharsConst = $countChars;
    $countArrItem = count_while($arr_lower);
    $countConst = $countArrItem;
    while($countChars != 0) 
    { 
        $countArrItem = $countConst;
        while ($countArrItem != 0) 
        { 
            if($str[$countCharsConst - $countChars] == $arr_lower[$countConst - $countArrItem])
            {
                $newStr .= $arr_upper[$countConst - $countArrItem];
            }
            elseif($str[$countCharsConst - $countChars] == ' ')
            {
                $newStr .= $str[$countCharsConst - $countChars];
            }
            elseif($str[$countCharsConst - $countChars] == $arr_upper[$countConst - $countArrItem])
            {
                $newStr .= $arr_upper[$countConst - $countArrItem];
            }     
            $countArrItem--;    
        }
        $countChars--;
    }
    return $newStr;
}
print str_to_upper_while($str6);

echo '<br>';

function str_to_upper_foreach(string $str)
{
    $arrChars = [
        'A' => 'a',
        'B' => 'b',
        'C' => 'c',
        'D' => 'd',
        'E' => 'e',
        'F' => 'f',
        'G' => 'g',
        'H' => 'h',
        'I' => 'i',
        'J' => 'j',
        'K' => 'k',
        'L' => 'l',
        'M' => 'm',
        'N' => 'n',
        'O' => 'o',
        'P' => 'p',
        'Q' => 'q',
        'R' => 'r',
        'S' => 's',
        'T' => 't',
        'U' => 'u',
        'V' => 'v',
        'W' => 'w',
        'X' => 'x',
        'Y' => 'y',
        'Z' => 'z'
    ];
    $newStr = '';
    $newArr = [];
    $countChars = str_count_while($str);
    for ($i=0; $i < $countChars; $i++)
    { 
        $newArr[] .= $str[$i];
    }
    foreach ($newArr as $item) 
    {
        foreach ($arrChars as $upper => $lower) 
        {
            if($item == $lower)
            {
                $newStr .= $upper;
            }
            elseif($item == ' ')
            {
                $newStr .= $item;
            }
            elseif($item == $upper)
            {
                $newStr .= $upper;
            }
        }      
    }
    return $newStr;
}
print str_to_upper_foreach($str6);

echo '<br>';

function str_to_upper_do_while(string $str)
{
    $arr_upper = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $arr_lower = ['a','b','c','d','e','f','g','h','i','g','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $newStr = '';
    $countChars = str_count_while($str);
    $countCharsConst = $countChars;
    $countArrItem = count_while($arr_lower);
    $countConst = $countArrItem;
    do
    { 
        $countArrItem = $countConst;
        do
        { 
            if($str[$countCharsConst - $countChars] == $arr_lower[$countConst - $countArrItem])
            {
                $newStr .= $arr_upper[$countConst - $countArrItem];
            }
            elseif($str[$countCharsConst - $countChars] == ' ')
            {
                $newStr .= $str[$countCharsConst - $countChars];
            }
            elseif($str[$countCharsConst - $countChars] == $arr_upper[$countConst - $countArrItem])
            {
                $newStr .= $arr_upper[$countConst - $countArrItem];
            }     
            $countArrItem--;    
        }while ($countArrItem != 0);
        $countChars--;
    }while($countChars != 0);
    return $newStr;
}
print str_to_upper_do_while($str6);

echo '<h3> 7. Дан массив [\'Alex\', \'Vanya\', \'Tanya\', \'Lena\', \'Tolya\'], сделать все буквы с маленькой. </h3>';
$array7 = ['Alex','Vanya','Tanya','Lena','Tolya'];
$array7_new1 = [];
$array7_new2 = [];
$array7_new3 = [];
$array7_new4 = [];
$c7 = count_for($array7);
$cc7 = $c7;
for ($i=0; $i < $c7; $i++) 
{ 
    $array7_new1[] .= str_to_lower_for($array7[$i]);
}
echo '<pre>';
print_r($array7_new1);
echo '</pre>';

while($c7 != 0)
{
    $array7_new2[] .= str_to_lower_while($array7[$cc7 - $c7]);
    $c7--;
}
echo '<pre>';
print_r($array7_new2);
echo '</pre>';

foreach($array7 as $word)
{
    $array7_new3[] .= str_to_lower_foreach($word);
}
echo '<pre>';
print_r($array7_new3);
echo '</pre>';

$c7 = $cc7;
do
{
    $array7_new4[] .= str_to_lower_do_while($array7[$cc7 - $c7]);
    $c7--;
}while($c7 != 0);
echo '<pre>';
print_r($array7_new4);
echo '</pre>';

echo '<h3> 8. Дан массив [\'Alex\', \'Vanya\', \'Tanya\', \'Lena\', \'Tolya\'], сделать все буквы с большой. </h3>';
$array8 = ['Alex','Vanya','Tanya','Lena','Tolya'];
$array8_new1 = [];
$array8_new2 = [];
$array8_new3 = [];
$array8_new4 = [];
$c8 = count_for($array8);
$cc8 = $c8;
for ($i=0; $i < $c8; $i++) 
{ 
    $array8_new1[] .= str_to_upper_for($array8[$i]);
}
echo '<pre>';
print_r($array8_new1);
echo '</pre>';

while($c8 != 0)
{
    $array8_new2[] .= str_to_upper_while($array8[$cc8 - $c8]);
    $c8--;
}
echo '<pre>';
print_r($array8_new2);
echo '</pre>';

foreach($array8 as $word)
{
    $array8_new3[] .= str_to_upper_foreach($word);
}
echo '<pre>';
print_r($array8_new3);
echo '</pre>';

$c8 = $cc8;
do
{
    $array8_new4[] .= str_to_upper_do_while($array8[$cc8 - $c8]);
    $c8--;
}while($c8 != 0);
echo '<pre>';
print_r($array8_new4);
echo '</pre>';

echo '<h3> 9. Дано число $num = 1234678, развернуть его в обратном направлении. </h3>';
$num = 12345678;

function int_reverse_for(int $number)
{
    $str = (string)$number;
    $newStr = '';
    $count1 = str_count_while($str);
    for ($i = $count1; $i > 0; $i--) 
    { 
        $newStr .= $str[$i-1];
    }
    return (int)$newStr;
}
echo int_reverse_for($num);

echo '<br>';

function int_reverse_while(int $number)
{
    $str = (string)$number;
    $newStr = '';
    $count = str_count_while($str);
    while($count != 0)
    {
        $newStr .= $str[$count-1];
        $count--;
    }
    return (int)$newStr;
}
echo int_reverse_while($num);

echo '<br>';

function int_reverse_foreach(int $number)
{
    $str = (string)$number;
    $arr = [];
    $str1 = '';
    $count1 = str_count_while($str);
    for ($i = $count1; $i > 0 ; $i--) 
    { 
        $arr[] .= $str[$i - 1];
    }
    foreach($arr as $n)
    {
        $str1 .= $n;
    }
    $nn = (int)$str1;
    return $nn;
}
echo int_reverse_foreach($num);

echo '<br>';

function int_reverse_do_while(int $number)
{
    $str = (string)$number;
    $newStr = '';
    $count = str_count_while($str);
    do
    {
        $newStr .= $str[$count-1];
        $count--;
    }while($count != 0);
    return (int)$newStr;
}
echo int_reverse_do_while($num);

echo '<h3> 10. Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69], отсортируй его в порядке убывания. </h3>';
$array10 =  [44, 12, 11, 7, 1, 99, 43, 5, 69];

function array_sort_for(array $arr)
{
    $count = count_for($arr);
    for ($i = 0; $i < $count; $i++) 
    { 
        for ($j = 0; $j < $count; $j++) 
        { 
            if($arr[$i] > $arr[$j])
            {
                $temp = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $temp;
            }
        }
    }
    return $arr;
}
echo '<pre>';
print_r(array_sort_for($array10));
echo '</pre>';

echo '<br>';

function array_sort_while(array $arr)
{
    $count = count_for($arr);
    $count2 = $count;
    $count1 = $count;
    while ($count1-- != 0) 
    { 
        $count2 = $count;
        while ($count2-- != 0) 
        { 
            if($arr[$count1] < $arr[$count2])
            {
                $temp = $arr[$count1];
                $arr[$count1] = $arr[$count2];
                $arr[$count2] = $temp;
            }
        }
    }
    return $arr;
}
echo '<pre>';
print_r(array_sort_while($array10));
echo '</pre>';

echo '<br>';

function array_sort_foreach(array $arr)
{
    $count = count_for($arr) - 1;
    foreach($arr as $number_1 => $value_1)
    {
        $e = true;
        foreach($arr as $number_2 => $value_2)
        {
            if($number_2 == $count)
            {
                break;
            }
            if($arr[$number_2] < $arr[$number_2 + 1])
            {
                list($arr[$number_2],$arr[$number_2 + 1]) = [$arr[$number_2 + 1],$arr[$number_2]];
                $e = false;
            }
        }
        if($e)
        {
            break;
        }
    }
    return $arr;
}
echo '<pre>';
print_r(array_sort_foreach($array10));
echo '</pre>';

echo '<br>';

function array_sort_do_while(array $arr)
{
    $count = count_for($arr);
    $count2 = $count;
    $count1 = $count;
    do 
    { 
        $count1--;
        $count2 = $count;
        do 
        { 
            $count2--;
            if($arr[$count1] < $arr[$count2])
            {
                $temp = $arr[$count1];
                $arr[$count1] = $arr[$count2];
                $arr[$count2] = $temp;
            }
        }while ($count2 != 0);
    }while ($count1 != 0);
    return $arr;
}
echo '<pre>';
print_r(array_sort_do_while($array10));
echo '</pre>';

?>