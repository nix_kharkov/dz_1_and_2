<?php 

function Lesson()
{
    // $name = "Виктор";
    // echo $name;
    // echo "<br>";
    // $name = 4;
    
    // echo $name;
    // echo "<br>";
    // $firstName = "Alex";
    // $pi = 3.32323;
    
    // echo 'Pi = '.$pi;
    // echo "<br>";
    // echo "Pi = {$pi}";
    
    // $test = 'Victor';
    
    // $arr = 'Alex';
    
    // var_dump(isset($arr));
    
    // if(isset($arr))
    // {
    //     echo 'Есть такая переменная';
    // }
}

$myName = 'Aleks Sukhoruchko';
$age = 18;
$pi = pi();
$arr = ['alex', 'vova','tolya'];
$arr1 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
$arr2 = ['alex','vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
$arr3 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
echo '<pre>';
echo '<h2> Все переменные выводим через echo/print/print_r  </h2>';
echo '</pre>';
echo '<pre>';
echo "<h2> Задание 1: создать переменную строку ваше имя и вывести на экран =-> {$myName}</h2>";
echo '</pre>';
echo '<pre>';
echo "<h2> Задание 2: создать переменную возраст и вывести на экран =-> {$age} </h2>";
echo '</pre>';
echo '<pre>';
echo "<h2> Задание 3: создать переменную и присвоить ей число pi вывести переменную на экран =-> {$pi} </h2>";
echo '</pre>';
echo '<pre>';
print "<h2> Задание 4: создать переменную массив [‘alex’, ‘vova’, ‘tolya’] вести ее на экран =-> </h2>";
echo '</pre>';
echo '<pre>';
print_r($arr);
echo '</pre>';
echo '<pre>';
echo '<h2> Задание 5: создать переменную массив [‘alex’, ‘vova’, ‘tolya’, [‘kostya’, ‘olya’]] вести ее на экран =-> </h2>';
echo '</pre>';
echo '<pre>';
print_r($arr1);
echo '</pre>';
echo '<pre>';
echo '<h2> Задание 6: создать переменную массив [‘alex’, ‘vova’, ‘tolya’, [‘kostya’, ‘olya’, [‘gosha’, mila]]] вести ее на экран =-> </h2>';
echo '</pre>';
echo '<pre>';
print_r($arr2);
echo '</pre>';
echo '<pre>';
echo '<h2> Задание 7: создать переменную массив [[‘alex’, ‘vova’, ‘tolya’], [‘kostya’, ‘olya’], [‘gosha’, mila]] вести ее на экран =-> </h2>';
echo '</pre>';
echo '<pre>';
print_r($arr3);
echo '</pre>';
?>

